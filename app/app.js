(function (window) {
    var app = angular.module('myApp', ['ngRoute']);
    app.config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider
                .when('/', {
                    title: 'current',
                    templateUrl: 'app/dashboard/dashboardview.html',
                    controller: 'CustController'
                })
                .when('/addressview', {
                    title: 'address',
                    templateUrl: 'app/address/addressview.html',
                    controller: 'addressController'
                })
                .otherwise({
                    redirectTo: '/current'
                });

        }]);
    window.app = app;
}(window));