app.controller('addressController', function ($scope) {
    $scope.empData = [{
            name: 'David',
            phone: '13468529',
            country: 'France'
        },
        {
            name: 'John',
            phone: '88774456',
            country: 'USA'
        },
        {
            name: 'Simon',
            phone: '11233689',
            country: 'Norway'
        },
        {
            name: 'Liza',
            phone: '88564664',
            country: 'Canada'
        },
        {
            name: 'Scarlet',
            phone: '12459876',
            country: 'Colombia'
        },
        {
            name: 'Paul',
            phone: '45612397',
            country: 'USA'
        },
        {
            name: 'Jennifer',
            phone: '13567895',
            country: 'USA'
        },
        {
            name: 'Kimi',
            age: '21',
            salary: '1000',
            sex: 'Female',
            phone: '55646466',
            country: 'USA'
        },
        {
            name: 'Roger',
            age: '25',
            salary: '750',
            sex: 'Male',
            phone: '45677789',
            country: 'Canada'
        },
        {
            name: 'Robin',
            phone: '45622122',
            country: 'South Africa'
        },
        {
            name: 'James',
            phone: '99995666',
            country: 'Australia'
        }
                    ];
});