app.controller('CustController', function ($scope) {
    $scope.empData = [{
            name: 'David',
            age: '24',
            salary: '900',
            sex: 'Male',
        },
        {
            name: 'John',
            age: '44',
            salary: '1400',
            sex: 'Male',
        },
        {
            name: 'Simon',
            age: '27',
            salary: '950',
            sex: 'Male',
        },
        {
            name: 'Liza',
            age: '28',
            salary: '1200',
            sex: 'Female',
        },
        {
            name: 'Scarlet',
            age: '22',
            salary: '600',
            sex: 'Female',
        },
        {
            name: 'Paul',
            age: '30',
            salary: '1500',
            sex: 'Male',
        },
        {
            name: 'Jennifer',
            age: '24',
            salary: '1100',
            sex: 'Female',
            phone: '13567895',
            country: 'USA'
        },
        {
            name: 'Kimi',
            age: '21',
            salary: '1000',
            sex: 'Female',
            phone: '55646466',
            country: 'USA'
        },
        {
            name: 'Roger',
            age: '25',
            salary: '750',
            sex: 'Male',
            phone: '45677789',
            country: 'Canada'
        },
        {
            name: 'Robin',
            age: '20',
            salary: '400',
            sex: 'Male',
        },
        {
            name: 'James',
            age: '35',
            salary: '1400',
            sex: 'Male',
        }
                    ];
});